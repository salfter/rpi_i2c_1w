rpi_i2c_1w
==========

I2C &amp; 1-Wire interface board for Raspberry Pi

This is version 2 of the hardware design, created with KiCad.  Version 1 was
created on Upverter; look at commit f19b0ab3 for more information on that.

Likewise, the DS2406 driver I wrote to make this work has long since been
incorporated into the Linux kernel.  Just make sure CONFIG_W1_SLAVE_DS2406
is enabled (you'll need to enable CONFIG_W1 first).

